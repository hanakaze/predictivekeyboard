MyK A -- predictive Keyboard
========================================================
author: Nan Jiang


Language Model
========================================================
This app uses a **trigram** language model for predictive task. 

##Assumption of Ngram Model: A word only depends on N-1 previous words, i.e.

$$P(w_i|w_{i-1}w_{i-2})$$

Pros of Trigram model: More powerful on prediction, benefits small corpus

Cons of Trigram model: Computational intensive compared to bigram model, however fast enough compared to deep learning.

Model Building
============================

## Interpolation Smoothing

Using Maximum-likely-hood estimation to estimate:

$$\tilde{P}(w_i|w_{i-1}w_{i-2}) = \lambda_{1} P(w_i|w_{i-1}w_{i-2}) + \lambda_{2} P(w_i|w_{i-1}) +(1-\lambda_1-\lambda_2) P(w_i)$$


## Package used: 
- **tm** for infrastructure
- **tex2vec** for speedy vectorization
- **likihood** for optimization
- **hash** for data storage optimazation

Ohter Implementation
========================================================
The most appropriate method for predictive text is **LSTM** (Implementation using mxnet package).
sample code

```r
# load.data
# replicate.data
# drop.tail
# get.label

train <- load.data('sample.txt')
X.train <- train$X
dic <- train$dic
```
***

```r
X.train.data <- replicate.data(X.train, seq.len)
vocab <- length(dic)

X.train.data <- drop.tail(X.train.data, batch.size)
X.train.label <- get.label(X.train.data)
X.train <- list(data=X.train.data, label=X.train.label)
model <- mx.lstm(X.train, ...)
```
This requires a lot of resources to run and my PC allows only ~10 hidden layers

App Usage Simple and Easy
========================================================

![app page1](pic1.png) 
***
![app page2](pic2.png)

Input as many words as you like and hit submit

