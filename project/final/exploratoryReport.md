---
title: "report"
author: "Nan"
date: "October 8, 2016"
output: html_document
---



## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:


```r
head(d3)
```

```
##                                    word freq
## presid barack obama presid barack obama  137
## new york citi             new york citi  101
## two year ago               two year ago   94
## gov chris christi     gov chris christi   85
## st loui counti           st loui counti   75
## first time sinc         first time sinc   59
```

## Including Plots

You can also embed plots, for example:

![plot of chunk pressure](figure/pressure-1.png)

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
