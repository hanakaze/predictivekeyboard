library(rnn)
library(tm)
library(dplyr)
library(hash)
library(data.table)
library(RWeka)

rr<- tm_map(data[2],content_transformer(function(x) gsub('[^a-zA-Z ]+', "", x))) %>% 
        tm_map(stripWhitespace) %>%
        tm_map(removePunctuation) %>%
        tm_map(content_transformer(tolower)) %>%
        tm_map(removeNumbers) %>%
        tm_map(removeWords, stopwords("english"))
rr <- tm_map(rr,stemDocument)


BigramTokenizer <- function(x) NGramTokenizer(x, Weka_control(min = 3, max = 3))

r1 <- TermDocumentMatrix(rr)
r3 <- TermDocumentMatrix(rr, control = list(tokenize = BigramTokenizer))

m1 <- as.matrix(r1)
v1 <- sort(rowSums(m1),decreasing=TRUE)
d1 <- data.frame(word = names(v1),freq=v1)

m3 <- as.matrix(r3)
v3 <- sort(rowSums(m3),decreasing=TRUE)
d3 <- data.frame(word = as.character(names(v3)),freq=v3)
